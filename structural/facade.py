"""
Implementations of Facade Design Pattern

hide complexities of system and provide an interface to the client


examples:

"""

class Client():
    """
    >>> c = Client()
    >>> c.use_facade()
    A
    B
    """
    def __init__(self):
        pass

    def use_facade(self):
        facade = Facade()
        facade.do_things()


class Facade():
    def __init__(self):
        pass

    def do_things(self):
        self.subsystemA = SubsystemA()
        self.subsystemA.doA()

        self.subsystemB = SubsystemB()
        self.subsystemB.doB()


class SubsystemA():
    def __init__(self):
        pass

    def doA(self):
        print("A")

class SubsystemB():
    def __init__(self):
        pass

    def doB(self):
        print("B")

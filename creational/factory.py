"""
Implementations of Factory Design Pattern

make objects of various types for client classes
provide simple interface to client
allow for objects to be reused

examples:
database adapters
"""
from abc import ABC, abstractmethod

class Child(ABC):
    @abstractmethod
    def do_something(self):
        pass

class ChildA(Child):
    def do_something(self):
        print("A")

class ChildB(Child):
    def do_something(self):
        print("B")


################################################################################


class SimpleFactory():
    """
    SimpleFactory creates object of whatever type is passed to it
    >>> factory = SimpleFactory()
    >>> factory.do_thing('ChildA')
    A
    """
    def do_thing(self, object_type):
        return eval(object_type)().do_something()


################################################################################


class FactoryMethod(ABC):
    """
    Factory Method delegates object creation to its subclasses
    creation is through inheritance, not instantiation
    code that creates object is separate from code that uses it
    used to create one product
    >>> parent = ParentA()
    >>> child = parent.get_children()[0]
    >>> child.do_something()
    A
    """
    def __init__(self):
        self.children = []
        self.create()

    @abstractmethod
    def create(self):
        pass

    def get_children(self):
        return self.children

    def add_children(self, child):
        self.children.append(child)

class ParentA(FactoryMethod):
    def create(self):
        self.add_children(ChildA())

class ParentB(FactoryMethod):
    def create(self):
        self.add_children(ChildB())


################################################################################


class AbstractFactory(ABC):
    """
    one or more factory methods to create a family of related objects
    uses composition to delegate responsibility of creating objects
    used to create family of related objects
    >>> t = ThingMaker()
    >>> t.make_things()
    A1
    2A
    B1
    2B
    """
    @abstractmethod
    def create1(self):
        pass

    @abstractmethod
    def create2(self):
        pass

class ThingMaker():
    def __init__(self):
        pass

    def make_things(self):
        for factory in [FactoryA(), FactoryB()]:
            thing1 = factory.create1()
            thing2 = factory.create2()
            thing1.do_something()
            thing2.do_something()


class FactoryA(AbstractFactory):
    def create1(self):
        return AThing1()

    def create2(self):
        return AThing2()

class FactoryB(AbstractFactory):
    def create1(self):
        return BThing1()

    def create2(self):
        return BThing2()

class Thing1(ABC):
    @abstractmethod
    def do_something(self):
        pass

class Thing2(ABC):
    @abstractmethod
    def do_something(self):
        pass

class AThing1(Thing1):
    def do_something(self):
        print("A1")

class AThing2(Thing2):
    def do_something(self):
        print("2A")

class BThing1(Thing1):
    def do_something(self):
        print("B1")

class BThing2(Thing2):
    def do_something(self):
        print("2B")

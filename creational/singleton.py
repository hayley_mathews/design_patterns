"""
Implementations of Singleton Design Pattern

ensure that one and only one object of class gets created
provide a global access point to object
control concurrent access to shared resources

examples:
logging class
database object
python module imports
"""

class Singleton():
    """
    Singleton class that creates new instance only if none is present
    >>> s1 = Singleton()
    >>> s2 = Singleton()
    >>> s1 is s2
    True
    """
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance


################################################################################


class MetaSingleton(type):
    """
    metaclass version of Singleton
    >>> class Logger(metaclass=MetaSingleton): pass
    >>> l1 = Logger()
    >>> l2 = Logger()
    >>> l1 is l2
    True
    """
    __instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.__instances:
            cls.__instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls.__instances[cls]


################################################################################


class BorgSingleton():
    """
    multiple instances, but single shared state between all
    >>> b1 = BorgSingleton()
    >>> b2 = BorgSingleton()
    >>> b1 is b2
    False
    >>> b1.x = 4
    >>> b1.__dict__ is b2.__dict__
    True
    """
    __shared_state = {"1": "2"}

    def __init__(self):
        self.__dict__ = self.__shared_state


################################################################################


class BorgSingletonNew():
    """
    multiple instances, but single shared state between all
    >>> b1 = BorgSingletonNew()
    >>> b2 = BorgSingletonNew()
    >>> b1 is b2
    False
    >>> b1.x = 4
    >>> b1.__dict__ is b2.__dict__
    True
    """
    __shared_state = {}

    def __new__(cls, *args, **kwargs):
        obj = super(BorgSingletonNew, cls).__new__(cls, *args, **kwargs)
        obj.__dict__ = cls.__shared_state
        return obj
